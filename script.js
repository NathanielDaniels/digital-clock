// const (constant) is a variable that can NOT be changed later on, which is why its called constant. If you plan to use a function to change the document, you must use "var" since "const" will produce an error. Below you will find both "const" and "var". Const is used to pull document elements to be edited in JS. Var is used inside the function which changes the document through ".innerHTML"

const clockHour = document.getElementById("hour");
const clockMinute = document.getElementById("minute");
const clockSecond = document.getElementById("second");

function clock() {
    var fullDate = new Date();
    var hours = fullDate.getHours();
    var minutes = fullDate.getMinutes();
    var seconds = fullDate.getSeconds();

    if (hours < 10) {
        hours = "0" + hours;
     } 
    if (minutes < 10) {
        minutes = "0" + minutes;
     }
    if (seconds < 10) {
        seconds = "0" + seconds;
     }
     
    clockHour.innerHTML = hours + ":";
    clockMinute.innerHTML = minutes + ":";
    clockSecond.innerHTML = seconds;
}

setInterval(clock, 1000);
//1000 miliseconds = 1 second



//console.log(fullDate.getHours());
//console.log(fullDate.getMinutes());
//console.log(fullDate.getSeconds());


//== Date =================================

//For some reason, wrapping this up in a function breaks the code. it works perfectly fine without function. Above, function might only need to be present due to "setInterval".

//function calander() {
    const dateMonth = document.getElementById("month");
    const dateDay = document.getElementById("day");
    const dateYear = document.getElementById("year");

    var wholeDate = new Date();
    var month = wholeDate.getMonth();
    var day = wholeDate.getDay();
    var year = wholeDate.getFullYear();

    if (month = "Oct") {
        month = 10;
    }

    dateMonth.innerHTML = month + " -"; 
    dateDay.innerHTML = day + " -";
    dateYear.innerHTML = year;
//}


//console.log(date.getMonth());
//console.log(date.getDay());
//console.log(date.getFullYear());
